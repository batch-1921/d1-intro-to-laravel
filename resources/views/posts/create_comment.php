@extends('layouts.app')

@section('content')
<div>
@if(Auth::id() == $post->user_id)
			<form class="d-inline" method="POST" action="/posts/{{$post->id}}/comment">
				@csrf
				<div class="form-group">
					<label for="comment">Comment:</label>
					<textarea class="form-control" id="" name="content" rows="3"></textarea>
				  </div>

				@if($post->comments->contains("user_id", Auth::id()))
			<button type="submit" class="btn btn-info">Comment </button>
			@endif
		</form>
		@endif
        </div>

@endsection