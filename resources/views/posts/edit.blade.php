@extends('layouts.app')

@section('content')
    <form method="POST" action="/posts/{{$post->id}}">
        @method('PUT')
        @csrf 
      <div class="form-group">
        <label for="title">Title:</label>
        <input type="text" class="form-control" id="title" name="title" value="{{$post->title}}">
      </div>
<div class="form-group">
        <label for="content">Content:</label>
        <textarea class="form-control" id="content" name="content" rows="3">{{$post->content}}</textarea>
      </div>
      <div class="mt-2">
      	<button type="submit" class="btn btn-primary">Update Post</button>
  	  </div>
    </form>
@endsection



{{-- 

@extends('layouts.app')

@section('content')
            <div class="card text-center mb-4">
                <div class="card-body">
                    <h4 class="card-title mb-3">Title:
                        <input type="text" id="title" name ="title"
                        value ="{{$post->title}}" width="100">
                    </h4>
                    <h5 class="card-text">Content: </h5>
                        <textarea rows="10" cols="35" id="content" name="content">{{$post->content}}</textarea>
                </div>
                    <div class="card-footer">
                        <form method="POST" action="/posts/{{$post->id}}">
                            @method('PUT')
                            @csrf
                            <a href="/posts" class="btn btn-primary">Update Post</a>
                        </form>
                    </div>
            </div>
@endsection  --}}