<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    public function welcome()
    {
        //retrieved 3 randomized posts
        $posts = Post::inRandomOrder()
            ->limit(3)
            ->get();

        return view('welcome')->with('posts', $posts);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        //if there is an authenticated user
        if(Auth::user()){
            //create a new Post object from the Post model
            $post = new Post;

            //define the properties of the $post object using received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            //get the id of the authenticated user and set it as the value of the user_id column
            $post->user_id = (Auth::user()->id);
            //save the post object to the database
            $post->save();

            return redirect('/posts');
        }else{
            return redirect('/login');
        }
    }

    // public function index()
    // {
    //     //get all posts from the database
    //     $posts = Post::all();
    //     return view('posts.index')->with('posts', $posts);
    // }

    public function myPosts()
    {
        //if the user is logged in
        if(Auth::user()){
            $posts = Auth::user()->posts; //retrieve the user's own posts

            return view('posts.index')->with('posts', $posts);
        }else{
            return redirect('/login');
        }
    }

    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return view('posts.edit')->with('post', $post);
    }

    //controller method: update
    public function update(Request $request, $id)
    //pass both the form data in the request, as well as the id of the post to be updated
    {
        $post = Post::find($id);
        if(Auth::user()->id == $post->user_id){
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();

        }
        return redirect('/posts');
    }

    public function destroy($id){
       $post = Post::find($id);
       if(Auth::user()->id == $post->user_id){
     $post->delete();

       }
    }
    
    public function index()
    {
        $posts = Post::where('isActive', true)->get();
        return view('posts.index')->with('posts', $posts);
    }
    
        public function archive($id){
            $post = Post::find($id);
            if(Auth::user()->id == $post->user_id){
                $post->isActive = false;
                $post->save();
            }
        return redirect('/posts');
        
    }

    public function like($id){
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        if($post->user_id != $user_id){
            if($post->likes->contains("user_id",$user_id)){
                PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
            }else{
                $postLike = new PostLike;
                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;
                $postLike->save();
            }
            return redirect("/posts/$id");
        }
    }
    public function comment($id)
    {
        $post = Post::find($id);
        if(Auth::user()->id == $post->user_id){
            $postComment = new PostComment;
            $postComment->post_id = $post->id;
            $postComment->user_id = $user_id;
            $postComment->save();
        }
        return redirect("/posts/$id");
    }




}
